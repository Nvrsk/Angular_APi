import { Http } from '@angular/http';
import { HttpModule} from '@angular/http';
import {Response } from '@angular/http';
import { Component, NgModule } from '@angular/core';
import 'rxjs/add/operator/map';


@NgModule({
  imports: [
    Http,
    HttpModule
  ],
  declarations: [ BodyComponent ],
  bootstrap:    [ BodyComponent ]
})




@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent  {

private apiURL='https://api.themoviedb.org/3/movie/now_playing?&api_key=6fa872e36a6f7c38fa9479938d80d880&append_to_response=videos,images';
public  data: any={};
public  items:any;
public  Id:any;
private reviewsList:any;
public  reviewData: any={};
public  reviews:any;
private reviewsURL: any;
private imagesList:any;
public  imageData: any={};
public  images:any;
private imagesURL: any;








constructor( private http: Http ) {
   this.getMovieList();
   this.getMovieData();





   

   }

   getMovieList(){
     return this.http.get(this.apiURL)
        .map(res=>{
          return res.json()
        })
     
   }





   getMovieData(){
     this.getMovieList().subscribe(data=>{
      this.items = data.results;
      console.log(data);
  
      
     })
     
   }
   selectedMovie: Items;
   
     onSelect(items: Items): void {
       this.selectedMovie = items;
       console.log(this.selectedMovie);
       this.Id = this.selectedMovie['id'];
       this.reviewsList = 'https://api.themoviedb.org/3/movie/'+this.Id+'/reviews?&api_key=6fa872e36a6f7c38fa9479938d80d880&append_to_response=videos,images' ;
       this.imagesList  = 'https://api.themoviedb.org/3/movie/'+this.Id+'/images?&api_key=6fa872e36a6f7c38fa9479938d80d880&language=en' ;
       
       this.reviewsURL = this.reviewsList;
       
       this.http.get(this.reviewsURL)
       .map(res=>{
        return res.json()
        
        
      })
      .subscribe(reviewData=>{
       this.reviews = reviewData.results;
       console.log(this.reviews);
   
       
      }) 

       console.log(this.reviewsURL);
       console.log(this.Id);

       this.imagesURL = this.imagesList;
       
       this.http.get(this.imagesURL)
       .map(res=>{
        return res.json()
        
        
      })
      .subscribe(imageData=>{
       this.images = imageData.posters;
       console.log(this.images);
   
       
      }) 

      console.log(this.imagesURL);
       
     }

}

export class Items {

     selectedMovie: Items;
   
     onSelect(items: Items): void {
       this.selectedMovie = items;
     }

  

}